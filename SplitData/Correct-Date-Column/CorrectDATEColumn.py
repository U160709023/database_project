# The purpose of this python file is to organize "DATE" column in database project
# Firstly, the problem is "DATE" column is not suitable for relation "file"
# Dates are in form of day-month-year  in the original data
# but it is not suitable for mysql. It is suitable If only if it is in form of year-month-day
# Also, this column has "dot" between days, months and years. But mysql does not accept this format of "DATE" type data.
# we have to put between days, months and years , score sign (-)
# To handle these problem,
# step 1: Find all number written before "dot" and after "dot" by using RE (Regular Expression) library in Python
# Step 2: put these information into an array by reversing whole pattern(day-month-year) to  year- month- day
# Step 3: Concatenate these time patterns and put between days, months and years, score sign (-)

import csv
import re

csv.register_dialect('myDialect',
                     delimiter=',',
                     quoting=csv.QUOTE_ALL,
                     skipinitialspace=True)
mySecondList = []  # we put this array days, months and years by reversing patterns.

myList = []
with open('ount.csv', 'r') as f:  # Opening our wrong data to read and make it correct
    reader = csv.reader(f, dialect='myDialect')
    for row in reader:
        Dates = str(row)
        Read = re.findall("\d", Dates)
        myList.append(Read)  # Put our data into array MyList
print(myList)  # we should see what kind of our pattern is.

SendMySecondList = (
        str(myList[2][4]) + str(myList[2][5]) + str(myList[2][6]) + str(myList[2][7]) + "-" +
        str(myList[2][2]) + str(myList[2][3]) + "-" +
        str(myList[2][0]) + str(myList[2][1])
)
for b in range(0, len(myList)):  # Firstly, we have to fill the CSV file with "null"
    mySecondList.append("null")
for x in range(0, 2127):

    if (len(myList[x]) == 0):  # to prevent exception IndexOutOfRange
        continue;
    SendMySecondListinFor = (str(myList[x][4]) + str(myList[x][5]) + str(myList[x][6]) + str(myList[x][7]) + "-" +
                             str(myList[x][2]) + str(myList[x][3]) + "-" +
                             str(myList[x][0]) + str(myList[x][1])
                             )  # the sent information into mylist is reversing in form of years-month-day by adding score sign between time pattern

    mySecondList[x] = SendMySecondListinFor  # every pattern is sending second list in true format.
print(SendMySecondListinFor)  # to control if we reach the end of array MyList - It should be 1998-06-06

with open("countr.csv", "w") as f2:  # opening cvs countr file to write our pattern

    for n in range(0, len(mySecondList)):
        yazıcı = csv.writer(f2)

        yazıcı.writerow([mySecondList[n]])  # writing our pattern in countr.cvs file
f.close()
f2.close()